#!/usr/bin/perl

$nom_fichier_tsp = $ARGV[0];
$nom_fichier_csv = $nom_fichier_tsp;
$nom_fichier_csv =~ s/\.tsp/\.csv/g;

# ouverture du fichier contenant la liste des pages a indexer
open (FIC_TSP, "$nom_fichier_tsp") || die "*** Erreur : Impossible d'ouvrir $nom_fichier_tsp\n";

open (FIC_CSV, ">$nom_fichier_csv") || die "*** Erreur : Impossible d'ouvrir $nom_fichier_csv\n";

$_ = <FIC_TSP>;
$_ = <FIC_TSP>;
$_ = <FIC_TSP>;
$ligne = <FIC_TSP>;
chop($ligne);
@dimension = split(/:/i, $ligne);
$dimension[1] =~ s/ //g;
print FIC_CSV "$dimension[1]\n";
$_ = <FIC_TSP>;

while (<FIC_TSP>)
{
	$ligne = $_;
	@coordonnes = split(/ /i, $ligne);
	shift(@coordonnes);
	$ligne = join(";", @coordonnes);
	print FIC_CSV $ligne;
}

close(FIC_CSV);

close(FIC_TSP);

