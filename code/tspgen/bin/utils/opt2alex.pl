#!/usr/bin/perl

$nom_fichier_opt = $ARGV[0];
$nom_fichier_alex = $nom_fichier_opt;
$nom_fichier_alex =~ s/\.opt/\.alex/g;

open (FIC_OPT, "$nom_fichier_opt") || die "*** Erreur : Impossible d'ouvrir $nom_fichier_opt\n";

open (FIC_ALEX, ">$nom_fichier_alex") || die "*** Erreur : Impossible d'ouvrir $nom_fichier_alex\n";

$_ = <FIC_OPT>;
$_ = <FIC_OPT>;
$_ = <FIC_OPT>;
$_ = <FIC_OPT>;
$_ = <FIC_OPT>;

while (<FIC_OPT>)
{
	$ligne = $_;
	chop($ligne);
	$ligne = $ligne - 1;
	print FIC_ALEX "$ligne-";
}

close(FIC_ALEX);

close(FIC_OPT);

