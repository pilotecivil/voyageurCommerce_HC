#!/usr/bin/perl
#-------------------------------------------------------------------
# generator.pl :
#	
# Auteur : Alexandre AUPETIT
# Date : 03/02/2001
# Licence : GPL
#-------------------------------------------------------------------

$nbcities = 100;

$ficname="circle"."$nbcities"."-cities.tsp";
open (FIC_TSP, ">$ficname") || die "Erreur : Impossible d'ouvrir le fichier $ficname !\n";
print FIC_TSP "$nbcities\n";
for ($i=0;$i<$nbcities;$i++)
{
	$pi=3.1415;
	$x=0.5+0.45*cos((2*$pi*$i)/($nbcities));
	$y=0.5+0.45*sin((2*$pi*$i)/($nbcities));
	print FIC_TSP "$x;$y\n";
}

close(FIC_TSP);


$nbcircles = 3;
$nb = 33;
$ficname="$nbcircles"."xcircle"."$nb"."-cities.tsp";
open (FIC_TSP, ">$ficname") || die "Erreur : Impossible d'ouvrir le fichier $ficname !\n";
$nbcities=$nbcircles*$nb;
print FIC_TSP "$nbcities" ."\n";
$pi=3.1415;
$factor=0.60;
$rayon=0.45;
for ($j=0;$j<$nbcircles;$j++)
{
	for ($i=0;$i<$nb;$i++)
	{
		$x=0.5+$rayon*cos((2*$pi*$i)/($nb));
		$y=0.5+$rayon*sin((2*$pi*$i)/($nb));
		print FIC_TSP "$x;$y\n";
	}
	$rayon=$rayon*$factor;
}

close(FIC_TSP);





$nbcircles = 10;
$nb = 40;
$ficname="$nbcircles"."xcircle"."$nb"."-cities.tsp";
open (FIC_TSP, ">$ficname") || die "Erreur : Impossible d'ouvrir le fichier $ficname !\n";
$nbcities=$nbcircles*$nb;
print FIC_TSP "$nbcities" ."\n";
$pi=3.1415;
$factor=0.95;
$rayon=0.45;
for ($j=0;$j<$nbcircles;$j++)
{
	for ($i=0;$i<$nb;$i++)
	{
		$x=0.5+$rayon*cos((2*$pi*$i)/($nb));
		$y=0.5+$rayon*sin((2*$pi*$i)/($nb));
		print FIC_TSP "$x;$y\n";
	}
	$rayon=$rayon*$factor;
}

close(FIC_TSP);










