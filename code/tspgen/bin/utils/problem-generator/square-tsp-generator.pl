#!/usr/bin/perl
#-------------------------------------------------------------------
# generator.pl :
#	
# Auteur : Alexandre AUPETIT
# Date : 03/02/2001
# Licence : GPL
#-------------------------------------------------------------------

$cote = 12;

$ficname="$cote"."x"."$cote"."cities.tsp";
open (FIC_TSP, ">$ficname") || die "Erreur : Impossible d'ouvrir le fichier $ficname !\n";
$nbcities=$cote*$cote;
print FIC_TSP "$nbcities\n";
for ($i=0;$i<$cote;$i++)
{
	$x=($i+1)/($cote+1);
	for ($j=0;$j<$cote;$j++)
	{
		$y=($j+1)/($cote+1);
		print FIC_TSP "$x;$y\n"
	}
}

close(FIC_TSP);
