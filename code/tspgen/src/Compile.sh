echo
echo
echo "==============================================================="
echo "================= Compilation de tspgen ======================="
echo "==============================================================="
make clean
make
echo
echo
echo "==============================================================="
echo "=============== Compilation de DisplayTsp ====================="
echo "==============================================================="
javac java/DisplayTsp.java
cp java/DisplayTsp.class ../bin/problem/optimum/
mv -f java/DisplayTsp.class ../bin/resultats/
