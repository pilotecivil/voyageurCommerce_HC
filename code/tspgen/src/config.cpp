/*==========================================================================
Project : tspgen 0.32
File    : config.cpp
Purpose : handling of parameters retrieved from a file
    Parameter is an int or a string
    Config is a set of Parameter
==========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"


/*========================================================================
============================== PARAM =====================================
==========================================================================*/

/*========================================================================
Function : Param constructor
In :   sName0 : name of the parameter
==========================================================================*/
Param::Param(char *sName0)
{
  strcpy(sName, sName0);
}

/*========================================================================
Function : Returns the name of the parameter
==========================================================================*/
char* Param::GetName()
{
  return sName;
}

/*========================================================================
Function : Sets the value of the Param (as a string)
In :   sValue0 : value
==========================================================================*/
void Param::SetStringValue(char *sValue0)
{
  strcpy(sValue, sValue0);
}

/*========================================================================
Function :  Sets the value of the Param (as an int)
In :   sValue0 : value
==========================================================================*/
void Param::SetIntValue(int iValue0)
{
  sprintf(sValue, "%d", iValue0);
}

/*========================================================================
Function : Param string accessor
==========================================================================*/
char* Param::GetStringValue()
{
  return sValue;
}

/*========================================================================
Function : Param int accessor
==========================================================================*/
int Param::GetIntValue()
{
  return atoi(sValue);
}

/*========================================================================
Function : Param bool accessor
==========================================================================*/
bool Param::GetBoolValue()
{
  bool bValue = false;

  if ( (strcmp(sValue, "1")==0) || (strcmp(sValue, "true")==0) || (strcmp(sValue, "y")==0)
        || (strcmp(sValue, "o")==0) || (strcmp(sValue, "vrai")==0) || (strcmp(sValue, "on")==0)
        || (strcmp(sValue, "Y")==0) || (strcmp(sValue, "O")==0) || (strcmp(sValue, "TRUE")==0)
        || (strcmp(sValue, "ON")==0) || (strcmp(sValue, "yes")==0) || (strcmp(sValue, "oui")==0) )
  {
    bValue = true;
  }

  return bValue;
}


/*========================================================================
============================== CONFIG ====================================
==========================================================================*/

/*========================================================================
Function : Config constructor
In :  sIniFileName : name of the .ini file
==========================================================================*/
Config::Config(char *sIniFileName)
{
  FILE* fIni;
  char sLine[MAX_LENLINE];
  char *sParam;
  char *sValue;
  int iIndex;

  // R�cup�ration du nb de param�tres
  iNbParam = 0;
  fIni = fopen(sIniFileName, "r");
  if (fIni != NULL)
  {
    while (fgets(sLine, MAX_LENLINE, fIni)!=NULL)
    {
      if ((sLine[0] != '#') && (sLine[0] != '['))
      {
        sParam=strtok(sLine, "=\n");
        if (sParam != NULL)
        {
          iNbParam++;
        }
      }
    }
    fclose(fIni);
  }
  else
  {
    printf("*** Erreur : Impossible de lire le fichier ini %s !\n", sIniFileName);
  }

  // Le premier param�tre est "vide"
  iNbParam++;
  // cr�ation de la liste des param�tres
  List = new Param*[iNbParam];

  // cr�ation du premier param�tre "vide"
  List[0] = new Param("***ERREUR***");
  List[0]->SetStringValue("0");

  // cr�ation et r�cup�ration des param�tres
  iIndex = 1;
  fIni = fopen(sIniFileName, "r");
  if (fIni != NULL)
  {
    while (fgets(sLine, MAX_LENLINE, fIni)!=NULL)
    {
      if ((sLine[0] != '#') && (sLine[0] != '['))
      {
        sParam=strtok(sLine, "=\n");
        sValue=strtok(NULL, "=#\t\n");

        if (sParam != NULL)
        {
          /* DEBUG printf("Param=<<%s>> Value=<<%s>>\n", sParam, sValue); */

          List[iIndex] = new Param(sParam);
          List[iIndex]->SetStringValue(sValue);
          iIndex ++;
        }
      }
    }
    fclose(fIni);
  }
  else
  {
    printf("*** ERREUR CRITIQUE : Impossible de lire le fichier ini !\n");
  }
}


/*========================================================================
Function : Config destructor
==========================================================================*/
Config::~Config()
{
  int i;

  for (i=0; i<iNbParam; i++)
  {
    delete List[i];
  }
  delete[] List;
}


/*========================================================================
Function : returns the index of the parameter
In : sParamName : the name of the parameter
==========================================================================*/
int Config::GetParamIndex(char *sParamName)
{
  int iIndex;
  bool bFound;

  iIndex = 1;
  bFound = false;

  while ((!bFound) && (iIndex<iNbParam))
  {
    if (strcmp(sParamName, List[iIndex]->GetName()) == 0)
    {
      bFound = true;
    }
    else
    {
      iIndex++;
    }
  }

  if (!bFound)
  {
    printf("*** Param�tre %s non trouv� !\n", sParamName);
    fflush(stdout);
    iIndex = 0;
  }

  return iIndex;
}

/*========================================================================
Function : returns the index of the named parameter
In : sParamName : the name of the parameter
==========================================================================*/
Param* Config::GetParamByName(char *sParamName)
{
  return List[GetParamIndex(sParamName)];
}

/*========================================================================
Function : returns the index of the indexed parameter
In : iIndex : the index of the parameter
==========================================================================*/
Param* Config::GetParam(int iIndex)
{
  return List[iIndex];
}
