/*==========================================================================
Project : tspgen - Resolution of the Traveling Salesman Problem
Date    : 07/04/2002
Version : 0.32
Author  : Alexandre AUPETIT <aaupetit@club-internet.fr>
          http://home.alex.tuxfamily.org
          Micha�l Trabbia <michael.trabbia@enst.fr> for alternative
          crossovers (thanks a lot !)
Copyleft : General Public Licence (GPL). See file COPYING.
          Comments/corrections/improvements are welcome !
==========================================================================*/

/*==========================================================================
Project : tspgen 0.32
File    : tspgen.cpp
Purpose : Main
    - Population is a set of Groups
    - Group is a set of Individual that evolves with
      selection/crossover/mutation/optimization
    - Individual is a particular path between all the cities of the Traveling
      Salesman Problem
==========================================================================*/
static char* _project_info    = "tspgen : Genetic Algorithms for the Traveling Salesman Problem";
static char* _project_version = "tspgen - v0.32 - 07/04/2002";
static char* _project_author  = "Alexandre Aupetit <aaupetit@club-internet.fr>\nMicha�l Trabbia <michael.trabbia@enst.fr>\n -- http://home.alex.tuxfamily.org/pvc.html";

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

#include "tspgen.h"


/*========================================================================
================================== MAIN ==================================
==========================================================================*/
int main (int argc, char *argv[])
{
  int i;
  char sInfo[255];
  char sIniFilename[255];
  char sTestLogName[255];
  int iNbTests;
  int iTestCount;

  srand (time (NULL));

  // r�cup�ration du nom du fichier ini
  if (GetLineParams(argc, argv, sIniFilename, &iNbTests))
  {
    Config *pConfig;
    // Lecture du fichier de config
    pConfig = new Config (sIniFilename);

    // boucle pour chaque test ("run")
    for (iTestCount=0; iTestCount<iNbTests; iTestCount++)
    {
      // on num�rote le fichier de logs s'il y a plusieurs tests
      if (iNbTests>1)
      {
        sprintf(sTestLogName, "%s-%03d", pConfig->GetParamByName ("LogBasename")->GetStringValue (), iTestCount);
      }
      else
      {
        strcpy(sTestLogName, pConfig->GetParamByName ("LogBasename")->GetStringValue ());
      }

      TspLog *pTspLog;
      // D�marrage des logs
      pTspLog = new TspLog (pConfig->GetParamByName ("LogLevel")->GetIntValue (),
                            pConfig->GetParamByName ("ScreenLogLevel")->GetIntValue (),
                            pConfig->GetParamByName ("LogDir")->GetStringValue (),
                            sTestLogName);
      pTspLog->StartFunction ("tspgen", 0, LEVEL_DEBUG, 0);

      // Affichage du n� de version
      pTspLog->ProjectInfoMsg(_project_info, _project_version, _project_author);

      // Affichage de tous les param�tres
      for (i=1; i<pConfig->iNbParam; i++)
      {
        sprintf(sInfo, "%s=%s", pConfig->GetParam(i)->GetName(), pConfig->GetParam(i)->GetStringValue());
        pTspLog->InfoMsg(sInfo);
      }

      // Ouverture du fichier TSP ou CSV (probl�me)
      TSP *pTSP;
      pTSP = new TSP (0,
                      pConfig->GetParamByName ("Problem")->GetStringValue (),
                      pConfig->GetParamByName ("NbNeighbour")->GetIntValue (),
                      pTspLog);


      // Cr�ation de la population des trajets � partir des donn�es du fichier ini
      Population *pP;
      pP = new Population (0,
                           pConfig->GetParamByName ("NbGroup")->GetIntValue (),
                           pConfig->GetParamByName ("NbIndividual")->GetIntValue (),
                           pTSP,
                           pTspLog,
                           pConfig->GetParamByName ("LogDir")->GetStringValue (),
                           pConfig->GetParamByName ("LogBasename")->GetStringValue ());

      // Initialisation de la population
      pP->Init (pConfig->GetParamByName ("Init")->GetStringValue ());

      // DEBUG
      //Test(pTSP, pP->List[0], pTspLog);

      // Calcul principal
      pP->Evolve (pConfig->GetParamByName ("Evol")->GetStringValue (),
                  pConfig->GetParamByName ("NbEvol")->GetIntValue (),
                  pConfig->GetParamByName ("GroupRepartition")->GetBoolValue ());
      delete pP;
      delete pTSP;

      pTspLog->EndFunction ("tspgen", 0);
      delete pTspLog;
    }
    delete pConfig;
  }

  return 0;
}

/*========================================================================
Function : Get the name of the ini file (tspgen.ini or -i inifilename)
In : int argc : nb of command line parameters
     char *argv[] : command line parameters
Out : sIniFilename : name of the ini file (default tspgen.ini)
      iNbTests : nb of tests (batch mode)
Returns : ok (no arg or -i finemme) or not ok
==========================================================================*/
bool GetLineParams (int argc, char *argv[], char *sIniFilename, int *iNbTests)
{
  bool bOk;
  int iPosIni, iPosTest;

  // nom par d�faut du fichier ini
  strcpy(sIniFilename, "tspgen.ini");
  bOk = false;
  *iNbTests = 1;
  iPosIni = -1;
  iPosTest = -1;

  // si pas d'arguments : ok
  if (argc == 1)
  {
    bOk = true;
  }
  // si -i nom_fichier ini : ok
  if ( (argc == 3) && (strcmp(argv[1], "-i")==0) )
  {
    bOk = true;
    iPosIni = 2;
  }
  // si -t nb_de_tests : ok
  if ( (argc == 3) && (strcmp(argv[1], "-t")==0) )
  {
    bOk = true;
    iPosTest = 2;
  }
  // si -i nom_fichier_ini -t nb_de_tests : ok
  if ( (argc == 5) && (strcmp(argv[1], "-i")==0) && (strcmp(argv[3], "-t")==0))
  {
    bOk = true;
    iPosIni = 2;
    iPosTest = 4;
  }
  // si -t nb_de_tests -i nom_fichier_ini : ok
  if ( (argc == 5) && (strcmp(argv[1], "-t")==0) && (strcmp(argv[3], "-i")==0))
  {
    bOk = true;
    iPosIni = 4;
    iPosTest = 2;
  }

  if (bOk)
  {
    // r�cup�ration de nom_fichier_ini
    if (iPosIni>0)
    {
      strcpy(sIniFilename, argv[iPosIni]);
    }
    // r�cup�ration de nb_de_tests
    if (iPosTest>0)
    {
      *iNbTests = atoi(argv[iPosTest]);
    }
  }

  if (!bOk)
  {
    Usage();
  }

  return bOk;
}

/*========================================================================
Function : Show the usage
==========================================================================*/
void Usage(void)
{
  printf("Usage :\n");
  printf("       tspgen [-i <inifilename>] [-t <nbtests>]\n");
}

