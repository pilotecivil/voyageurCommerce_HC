/*==========================================================================
Project : tspgen 0.32
File    : TSP.cpp
Purpose : Population, Group & Individual class header
==========================================================================*/

#ifndef TSPGEN_H
#define TSPGEN_H

#include "TSP.h"
#include "tsplog.h"
#include "config.h"
#include "Population.h"


void Test(TSP *pTSP, Group * pGroup, TspLog *pTspLog);
bool GetLineParams (int argc, char *argv[], char *sIniFilename, int *iNbTests);
void Usage(void);


#endif //GENETIC_H
