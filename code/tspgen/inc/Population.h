/*==========================================================================
Project : tspgen 0.32
File    : Population.h
Purpose : Population class header
==========================================================================*/

#ifndef POPULATION_H
#define POPULATION_H

#include "TSP.h"
#include "tsplog.h"
#include "Group.h"

/*========================================================================
Class : Population
==========================================================================*/
class Population
{
  public :
    Population(int iInstance0, int iNbGroup, int iNbIndividual, TSP* pTSP, TspLog *pTspLog0, char *sLogDir, char *sLogBasename);
    ~Population();

    Group **List;
    int iInstance; // num�ro d'instance (pour les logs)
    TspLog *pTspLog;
    int iPopulationSize;

    void Init(char *sInitMacro);
    void Evolve(char *sEvolMacro, int iNbCycles, bool bGroupRepartition);
    void Sort();
    void WriteToCSV();

  protected :
    int iGroupCount;
};

bool DecodeMacro(char *sMacro0, int iMacroNum, char *sFunctionName, int *iParam1, int *iParam2, int *iParam3, int *iParam4);
int GetValue(char *sValue);


#endif // POPULATION_H
