/*==========================================================================
Project : tspgen 0.32
File    : config.h
Purpose : handling of parameters retrieved from a file
    Parameter is an int or a string
    Config is a set of Parameter
==========================================================================*/

#ifndef CONFIG_H
#define CONFIG_H

#define MAX_LENLINE 1024

/*========================================================================
Class : Param
==========================================================================*/
class Param
{
  public :
    Param(char *sName0);
    char* GetName();
    void SetStringValue(char *sValue0);
    void SetIntValue(int iValue0);
    char* GetStringValue();
    int GetIntValue();
    bool GetBoolValue();

  private :
    char sName[255];
    char sValue[1024];
};

/*========================================================================
Class : Config
==========================================================================*/
class Config
{
  public :
    int iNbParam;
    Config(char *sIniFileName);
    ~Config();
    Param* GetParamByName(char *sParamName);
    Param* GetParam(int iIndex);

  private :
    Param **List;
    int GetParamIndex(char *sParamName);
};

bool DecodeMacro(char *sMacro0, int iMacroNum,
                 char *sFunctionName, int *iParam1, int *iParam2, int *iParam3, int *iParam4);


#endif // CONFIG_H
