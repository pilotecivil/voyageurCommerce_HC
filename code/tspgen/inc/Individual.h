/*==========================================================================
Project : tspgen 0.32
File    : Individual.h
Purpose : Individual class header
==========================================================================*/

#include "Group.h"

#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include "TSP.h"
#include "tsplog.h"
#include "config.h"

// Types de mutation
#define MUT_2_CHANGE 0
#define MUT_NEAR_2_CHANGE 1
#define MUT_2_SWAP 2
#define MUT_NEAR_2_SWAP 3
#define MUT_NEAR_2_KFP 4
#define MUT_NEAR_SCRAMBLE_SWAP 5
#define MUT_NEAR_SCRAMBLE_CHANGE 6
#define MUT_4_CHANGE 7
#define MUT_DEPL_1 8
#define MUT_DEPL_2 9
#define MUT_DEPL_4 10
#define MUT_PHEROM_1 12
#define MUT_PHEROM_2 13
#define MUT_REPLI 15

class Group;

/*========================================================================
Class : Individual
==========================================================================*/
class Individual
{
  public :
    Individual(TSP* pTSP, Group *pGroup0, TspLog *pTspLog0);
    ~Individual();

    Individual & operator=(const Individual & I)
    {
      Duplicate(I);
      return (*this);
    }

    TSP *pTSP;
    Group *pGroup;
    TspLog *pTspLog;
    double lfFitness;
    int *Cities;
    int iNbCities;

    void Duplicate (const Individual & I);
    void RandomInit ();
    void NearestNeighbourInit (Individual & I_temp, int iStartCity);
    void Eval ();
    void Read (FILE* fGroup);
    void Write (FILE* fGroup);
    void LogRecord (int iCurrentIteration);
    int FindCityPos (int iCity);
    int FindNextUnvisitedCity (int iCurrentCity, Individual & I_temp);
    void ExchangeCities (int iCity1, int iCity2);
    void ReverseCities (int i, int j);
    void Depl1Cities (int i, int j);
    void Depl1CitiesInv (int i, int j);
    void Depl2Cities (int i, int j);
    void Depl2CitiesInv (int i, int j);
    void Crossover (Individual & I1, Individual & I2, Individual & I_temp, int iCrossoverType);
    void CrossoverKFP (Individual & I1, Individual & I2, Individual & I_temp);
    void CrossoverPMX (Individual & I1, Individual & I2, Individual & I_temp);
    void CrossoverOX (Individual & I1, Individual & I2, Individual & I_temp);
    void CrossoverCX (Individual & I1, Individual & I2, Individual & I_temp);
    void CrossoverAA (Individual & I1, Individual & I2, Individual & I_temp);
    void Mutate (int iType);
    bool Optimize ();
    void OptimizeFinal ();
    bool Optimize2ChangeCompl ();
    bool Optimize2Special ();
    bool Optimize1DeplCompl ();
    bool Optimize2DeplCompl ();
    bool OptimizeSwapCompl ();
    bool Optimize2ChangeNear ();
    bool Optimize4ChangeNearLocal (int iCentre);
    double Gain2Change (int i, int j);
    double Gain1Depl (int i, int j);
    double Gain1DeplInv (int i, int j);
    double Gain2Depl (int i, int j);
    double Gain2DeplInv (int i, int j);
    double GainPherom (int i, int j);
};


#endif // INDIVIDUAL_H
