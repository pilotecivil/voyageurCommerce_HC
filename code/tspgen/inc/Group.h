/*==========================================================================
Project : tspgen 0.32
File    : Group.h
Purpose : Group class header
==========================================================================*/

#ifndef GROUP_H
#define GROUP_H

#include "TSP.h"
#include "tsplog.h"
#include "config.h"
#include "Individual.h"

/*========================================================================
Class : Group
==========================================================================*/
class Group
{
  public :
    Group(int iInstance0, int iNbIndividual, TSP* pTSP, TspLog *pTspLog0, char *sGroupfilename);
    ~Group();

    int iInstance; // num�ro d'instance (pour les logs)
    TspLog *pTspLog;
    Individual **List;
    int *SelectionArray;
    Individual *pI;
    Individual *pI_Temp;
    int *aPheromone;
    bool bPheromoneOk;
    int iGroupSize;
    int iDebutCalcul;
    char sGroupFilename[255];

    void RandomEvol(int iNbIndividual);
    void DarwinEvol(int iNbIndividual, int iNbMutationOptimization, int iNbMaxMutation, int iCrossoverType);
    int Reinsert(Individual & I);
    void InitSelectionArray();
    void DeleteSelectionArray();
    int GetRandSelectionArrayIndex();
    void Select(int *a, int *b);
    void SelectOne(int *a);
    bool Belongs(Individual & I);
    void Exchange(int iIndex1, int iIndex2);
    void RandomInit();
    void OptInit();
    void NearestNeighbourInit();
    void MonteCarloEvol(int iNbIndividual, int iNbMutation);
    void MonteCarloDistinctEvol(int iNbIndividual, int iNbMutation);
    void WriteToCSV();
    void ReadFromCSV();
    void Sort();
    void LocalSearchEvol();
    bool MonteCarloOptimization(float lfMinScore, int iMutationType, int iNbMutationOptimization, int iNbMaxMutation);
    void Duplicate(Group *G);
    void MakePheromoneMatrix();

  protected :
    Individual *pI2;
    int iListCount;
    FILE* fLogHtml;
    FILE* fLogTime;
};



#endif // GROUP_H
