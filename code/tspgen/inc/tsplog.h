/*==========================================================================
Project : tspgen 0.32
File    : TSP.cpp
Purpose : Population, Group & Individual class header
==========================================================================*/

#ifndef TSPLOG_H
#define TSPLOG_H

#define LEVEL_CRITICAL 0
#define LEVEL_INFO 1
#define LEVEL_DEBUG 2

/*========================================================================
Class : TspLog
==========================================================================*/
class TspLog
{
  public :
    TspLog(int iLogLevel0, int iScreenLogLevel0, char *sLogDir, char *sBaseLogName);
    ~TspLog();

    void StartFunction(char *sFunctionName0, int iInstance0, int iCurrentLevel0, int iNbIteration0);
    void EndFunction(char *sFunctionName0, int iInstance0);
    void LogRecord(int iCurrentIteration, char *sPath, double lfFitness, int iNbCities, bool bDefaultProblem, bool bTSPLIB, char *sCitiesPosX, char *sCitiesPosY);
    void DebugMsg(char *sMessage);
    void InfoMsg(char *sMessage);
    void ErrorMsg(char *sMessage);
    void ScreenMsg(char *sMessage);
    int TimeLog(double lfFitness);
    void ProjectInfoMsg(char *_project_info, char *_project_version, char *_project_author);
    void CreateGnuPlotFile(char *sLogDir, char *sTimeLogName);

  private :
    FILE* fGlobalLog;
    FILE* fTimeLog;
    FILE* fHtmlLog;

    int iStartLogTime;
    int iCurrentLogTime;
    int iStartTime;
    int iEndTime;
    int iNiveau;

    int iCurrentLevel;
    char sCurrentMessage[10000]; // RISQUE ****************************
    char sFunctionName[255];
    char sNiveau[255];
    int iNbIteration;

    int iLogLevel;
    int iScreenLogLevel;

    void GlobalLog();
    void CalculeChaineNiveau();
};


#endif //TSPLOG_H
