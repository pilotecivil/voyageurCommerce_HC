/*==========================================================================
Project : tspgen 0.32
File    : TSP.cpp
Purpose : The Traveling Salesman class header
==========================================================================*/

#ifndef TSP_H
#define TSP_H

#include "tsplog.h"

#define MAX_TSP_LENLINE 2047
#define MAX_DISPLAYED_CITIES 25


/*========================================================================
Class : TSP
==========================================================================*/
class TSP
{
  public :
    TSP(int iInstance0, char *sTSPfilename, int iNbNear0, TspLog *pTspLog0);
    TSP(int iInstance0, int iNbCities0, int iNbNear0, TspLog *pTspLog0);
    ~TSP();

    int iInstance;
    int iNbCities;
    int iNbNear;
    bool bTSPLIB;
    bool bDefaultProblem;

    double Distance(int i, int j);
    void Display();
    void WriteToCSV(char *sTSPfilename);
    void WriteToPosXY();
    char *getCitiesPosX();
    char *getCitiesPosY();

    int Near(int i, int j);
    int IndexNear(int iCity1, int iCity2);

    TspLog *pTspLog;

  protected :
    double *CitiesPosX;
    double *CitiesPosY;
    char sCitiesPosX[MAX_TSP_LENLINE*10];
    char sCitiesPosY[MAX_TSP_LENLINE*10];

    // tableau des distances
    double *aDistance;

   // tableau des villes proches
    int *aNear;
    // tableau des distances villes proches

    void FetchDistance();
    void FetchNear();
    void Stats();
};

#endif // TSP_H
