# -*- coding: iso-8859-1 -*-

# Via ordre des villes de l'algo g�n�tique et fichier de coordonn�es, calcules itin�raire cyclable via Google Maps Directions API
# et cr�e la trace GPX correspondante (globale + par �tape) et le fichier summary
# input : ordre des villes; coordonn�es GPS
# output : traces gpx et fichier summary

import simplejson, urllib
import re
import csv

header ='<?xml version="1.0" encoding="UTF-8"?>\n<gpx creator="Antoine Courcelles" version="1.1"\nxmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n<metadata><name><![CDATA[ Atelier HC _ Voyageur commerce]]></name>\n</metadata>\n'
footer = '</gpx>\n'

def write_gpx_track_summary(st):

	fic_gpx = open('tracks/gpx_summary.gpx', 'w')
	fic_gpx.write(header)
	fic_gpx.write(st)
	fic_gpx.write(footer)
	fic_gpx.close()
	

def write_gpx_track(result, nb, descr):
	nb_steps = len(result)
	
	track = '<trk><name><![CDATA[' + descr + ']]></name>\n<trkseg>\n'
	track += '<trkpt lat="' + str(result[0]['start_location']['lat']) + '" lon="'+ str(result[0]['start_location']['lng']) + '"></trkpt>\n'
	for s in result:
		track += '<trkpt lat="' + str(s['end_location']['lat']) + '" lon="'+ str(s['end_location']['lng']) + '"></trkpt>\n'

	track += '</trkseg></trk>'
	
	fic_gpx = open('tracks/gpx_' + str(nb) + '.gpx', 'w')
	fic_gpx.write(header)
	fic_gpx.write(track)
	fic_gpx.write(footer)
	fic_gpx.close()

	return track



#key = '' # key Google API

# Liste coordonnes GPS des Ateliers
coord_hc =  open('../data/atelier_fr_input_map.csv', 'rb')
coord = csv.reader(coord_hc, delimiter=';', quotechar='|')

list_coord = [];
coord.next() # passe la premiere ligne
# Extrait donnees
for row in coord:
	list_coord.append([row[0], row[1], row[2]])


# Resultats algo genetique
order_hc =  open('../resultats/OrdreFranceateliers_algo.csv', 'rb')
order_atelier = csv.reader(order_hc, delimiter='-', quotechar='|')

order_atelier.next() # passe la premiere ligne
row = order_atelier.next() # garde que la deuxieme (meilleur resultat)
cpt_track  = 0

sum_km = 0
sum_h = 0
fic_summary = open('summary.txt', 'w')
track_st = ''


row = row[2:len(row)-1] # supprimes distance et caractère vide
row.append(row[0]) # rajoute le premier à la fin pour boucle

for col in range(0, len(row)-1):

	ADR1 = list_coord[int(row[col])][1] + ',' + list_coord[int(row[col])][2]	
	ADR2 = list_coord[int(row[col+1])][1] + ',' + list_coord[int(row[col+1])][2]
	traj_step = str(list_coord[int(row[col])][0]) + ' - ' +  str(list_coord[int(row[col+1])][0])

	url = "https://maps.googleapis.com/maps/api/directions/json?origin={0}&destination={1}&mode=bicycling&key={2}".format(str(ADR1), str(ADR2), str(key))
	result = simplejson.load(urllib.urlopen(url))

	if result['status'] ==  'OK':
		km_step = result['routes'][0]['legs'][0]['distance']['value']/1000
		h_step = result['routes'][0]['legs'][0]['duration']['value']/60
		sum_km += km_step
		sum_h += h_step

	
		st = 'Etape ' + str(cpt_track) + ' ' + traj_step + '\n'
		st += 'Km : ' + str(km_step) + ' / Km Cumulés : ' + str(sum_km) + '\n'
		st += 'Temps: ' + str(h_step) + ' / Temps Cumulé : ' + str(sum_h) + '\n'
		st += ' ************************************************** \n'

		print st
		fic_summary.write(st)

		track_st += write_gpx_track(result['routes'][0]['legs'][0]['steps'], cpt_track, traj_step + ' / ' + str(km_step) + ' km / ' + str(h_step) + ' min')
	else:
		
		print 'ERROR'
		print traj_step
		print result

	cpt_track +=1


write_gpx_track_summary(track_st)
		
fic_summary.close()
