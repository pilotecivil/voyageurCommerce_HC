# -*- coding: iso-8859-1 -*-

#Calcule la matrice des distances 2 � 2 via Google Maps API Distance Matrix
#Input : fichier de coordonn�es CSV des ateliers
#Output : matrice des distances

import simplejson, urllib
import re
import csv

#key = '' => google API key



# Recup liste coordonnees
coord_hc =  open('../data/atelier_fr_input_algo.csv', 'r')
coord = csv.reader(coord_hc, delimiter=';', quotechar='|')

list_coord = []
cpt = 0
for row in coord:
	if cpt !=0:
		list_coord.append([row[0], row[1]])
	cpt+=1

# fichier final
output = open("matrix_distance.txt", "w")

for i in range(0, len(list_coord)):
	str_l = '';

	ADR1 = list_coord[i][0] + ',' + list_coord[i][1]

	for j in range(0,len(list_coord)):
		
		ADR2 = list_coord[j][0] + ',' + list_coord[j][1]
		
		url="https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&mode=bicycling&language=fr-FR&key={2}".format(str(ADR1), str(ADR2), str(key))
		result=simplejson.load(urllib.urlopen(url))
		
		if result['rows'][0]['elements'][0]['status']!='NOT_FOUND':
			dist= result['rows'][0]['elements'][0]['distance']['value']/1000.0 # en km
		else: # si marche pas, distance fausse
			dist = 99999999999

		str_l += str(dist) + ' '
		print i, j, dist

	output.write(str_l+'\n')



