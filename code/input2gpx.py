# -*- coding: iso-8859-1 -*-


# input2GPX.py
# input : resultat en csv pour l'ordre des villes : format - distance totale - ville 1 - ville 2 -...
# input : liste des Ateliers et coordonees gps en csv; format : nbVille\n lat; long\n...
# output : fichier GPX en ordre

import csv

# Liste coordonnes GPS des Ateliers
coord_hc =  open('../data/atelier_fr_input_map.csv', 'rb')
coord = csv.reader(coord_hc, delimiter=';', quotechar='|')

list_coord = [];
coord.next() # passe la premiere ligne
# Extrait donnees
for row in coord:
	list_coord.append([row[0], row[1], row[2]])


# Resultats algo genetique
order_hc =  open('../resultats/OrdreFranceateliers_algo.csv', 'rb')
order_atelier = csv.reader(order_hc, delimiter='-', quotechar='|')

fic_gpx = open('trace.gpx', 'w')
# Header GPS
fic_gpx.write('<?xml version="1.0" encoding="UTF-8"?>\n<gpx creator="Antoine Courcelles" version="1.1"\nxmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n\n<metadata><name><![CDATA[Ateliers_HC]]></name>\n</metadata>\n<trk><name><![CDATA[Atelier_HC]]></name><trkseg>\n')

order_atelier.next() # passe la premiere ligne
row = order_atelier.next() # garde que la deuxieme (meilleur resultat)
for col in row[2:-1]:
	fic_gpx.write('<trkpt lat="' + str(list_coord[int(col)][1]) + '" lon="'+ str(list_coord[int(col)][2]) + '"></trkpt>\n')  


fic_gpx.write('</trkseg></trk>\n</gpx>\n');





