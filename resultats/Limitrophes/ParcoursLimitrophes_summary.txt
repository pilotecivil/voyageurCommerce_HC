Etape 0 uN p'Tit véLo dAnS la Tête - Les Cycl’Hauts du Rabot
Km : 6 / Km Cumulés : 6
Temps: 28 / Temps Cumulé : 28
 ************************************************** 
Etape 1 Les Cycl’Hauts du Rabot - Le squat Garave à Fontaine
Km : 8 / Km Cumulés : 14
Temps: 34 / Temps Cumulé : 62
 ************************************************** 
Etape 2 Le squat Garave à Fontaine - Les Déraillées
Km : 1 / Km Cumulés : 15
Temps: 4 / Temps Cumulé : 66
 ************************************************** 
Etape 3 Les Déraillées - uN p'Tit véLo dAnS la Tête
Km : 2 / Km Cumulés : 17
Temps: 11 / Temps Cumulé : 77
 ************************************************** 
Etape 4 uN p'Tit véLo dAnS la Tête - Un vélo de plus
Km : 1 / Km Cumulés : 18
Temps: 6 / Temps Cumulé : 83
 ************************************************** 
Etape 5 Un vélo de plus - La Citrouille
Km : 0 / Km Cumulés : 18
Temps: 3 / Temps Cumulé : 86
 ************************************************** 
Etape 6 La Citrouille - Atelier Vélo d’Échirolles
Km : 5 / Km Cumulés : 23
Temps: 18 / Temps Cumulé : 104
 ************************************************** 
Etape 7 Atelier Vélo d’Échirolles - Atelier Cyclonique
Km : 111 / Km Cumulés : 134
Temps: 546 / Temps Cumulé : 650
 ************************************************** 
Etape 8 Atelier Cyclonique - Mobil'Idées
Km : 88 / Km Cumulés : 222
Temps: 318 / Temps Cumulé : 968
 ************************************************** 
Etape 9 Mobil'Idées - Re-Cycle-Art
Km : 63 / Km Cumulés : 285
Temps: 277 / Temps Cumulé : 1245
 ************************************************** 
Etape 10 Re-Cycle-Art - Au Tour du Cycle
Km : 78 / Km Cumulés : 363
Temps: 361 / Temps Cumulé : 1606
 ************************************************** 
Etape 11 Au Tour du Cycle - Je p'neu le faire
Km : 44 / Km Cumulés : 407
Temps: 133 / Temps Cumulé : 1739
 ************************************************** 
Etape 12 Je p'neu le faire - Maison du Vélo
Km : 47 / Km Cumulés : 454
Temps: 148 / Temps Cumulé : 1887
 ************************************************** 
Etape 13 Maison du Vélo - Kazacycle
Km : 21 / Km Cumulés : 475
Temps: 72 / Temps Cumulé : 1959
 ************************************************** 
Etape 14 Kazacycle - Osez l'vélo
Km : 77 / Km Cumulés : 552
Temps: 295 / Temps Cumulé : 2254
 ************************************************** 
Etape 15 Osez l'vélo - Planète Vélo Animation
Km : 33 / Km Cumulés : 585
Temps: 106 / Temps Cumulé : 2360
 ************************************************** 
Etape 16 Planète Vélo Animation - La P’tite Rustine
Km : 6 / Km Cumulés : 591
Temps: 25 / Temps Cumulé : 2385
 ************************************************** 
Etape 17 La P’tite Rustine - Le Cyclub
Km : 4 / Km Cumulés : 595
Temps: 14 / Temps Cumulé : 2399
 ************************************************** 
Etape 18 Le Cyclub - Les Bikers
Km : 4 / Km Cumulés : 599
Temps: 17 / Temps Cumulé : 2416
 ************************************************** 
Etape 19 Les Bikers - Change de Chaîne
Km : 7 / Km Cumulés : 606
Temps: 25 / Temps Cumulé : 2441
 ************************************************** 
Etape 20 Change de Chaîne - Le Recycleur
Km : 3 / Km Cumulés : 609
Temps: 15 / Temps Cumulé : 2456
 ************************************************** 
Etape 21 Le Recycleur - L'atelier vélo autogéré du chat perché
Km : 2 / Km Cumulés : 611
Temps: 12 / Temps Cumulé : 2468
 ************************************************** 
Etape 22 L'atelier vélo autogéré du chat perché - Le Recycleur
Km : 3 / Km Cumulés : 614
Temps: 13 / Temps Cumulé : 2481
 ************************************************** 
Etape 23 Le Recycleur - Atelier Vélo de Pierre-Bénite
Km : 5 / Km Cumulés : 619
Temps: 25 / Temps Cumulé : 2506
 ************************************************** 
Etape 24 Atelier Vélo de Pierre-Bénite - En rue libre
Km : 25 / Km Cumulés : 644
Temps: 101 / Temps Cumulé : 2607
 ************************************************** 
Etape 25 En rue libre - Atelier Le Dérailleur
Km : 27 / Km Cumulés : 671
Temps: 112 / Temps Cumulé : 2719
 ************************************************** 
Etape 26 Atelier Le Dérailleur - L'Hotel des Vil-e-s
Km : 143 / Km Cumulés : 814
Temps: 502 / Temps Cumulé : 3221
 ************************************************** 
Etape 27 L'Hotel des Vil-e-s - VéloBouloDodo
Km : 2 / Km Cumulés : 816
Temps: 11 / Temps Cumulé : 3232
 ************************************************** 
Etape 28 VéloBouloDodo - Association Tous Deux Roues
Km : 0 / Km Cumulés : 816
Temps: 3 / Temps Cumulé : 3235
 ************************************************** 
Etape 29 Association Tous Deux Roues - Ressourcerie Court-Circuit
Km : 87 / Km Cumulés : 903
Temps: 379 / Temps Cumulé : 3614
 ************************************************** 
Etape 30 Ressourcerie Court-Circuit - Mon Cher Vélo
Km : 149 / Km Cumulés : 1052
Temps: 485 / Temps Cumulé : 4099
 ************************************************** 
Etape 31 Mon Cher Vélo - La Cantine
Km : 64 / Km Cumulés : 1116
Temps: 207 / Temps Cumulé : 4306
 ************************************************** 
Etape 32 La Cantine - Axe&Cycle
Km : 186 / Km Cumulés : 1302
Temps: 609 / Temps Cumulé : 4915
 ************************************************** 
Etape 33 Axe&Cycle - Unis Vers …
Km : 101 / Km Cumulés : 1403
Temps: 340 / Temps Cumulé : 5255
 ************************************************** 
Etape 34 Unis Vers … - Dynamo, l'atelier du vélo
Km : 121 / Km Cumulés : 1524
Temps: 395 / Temps Cumulé : 5650
 ************************************************** 
Etape 35 Dynamo, l'atelier du vélo - Prenons le guidon
Km : 64 / Km Cumulés : 1588
Temps: 196 / Temps Cumulé : 5846
 ************************************************** 
Etape 36 Prenons le guidon - L'outil
Km : 236 / Km Cumulés : 1824
Temps: 815 / Temps Cumulé : 6661
 ************************************************** 
Etape 37 L'outil - Ateliers de la rue Voot
Km : 61 / Km Cumulés : 1885
Temps: 209 / Temps Cumulé : 6870
 ************************************************** 
Etape 38 Ateliers de la rue Voot - Ecole Claire Ville
Km : 6 / Km Cumulés : 1891
Temps: 26 / Temps Cumulé : 6896
 ************************************************** 
Etape 39 Ecole Claire Ville - 123Vélo
Km : 3 / Km Cumulés : 1894
Temps: 15 / Temps Cumulé : 6911
 ************************************************** 
Etape 40 123Vélo - Cycloperativa
Km : 2 / Km Cumulés : 1896
Temps: 7 / Temps Cumulé : 6918
 ************************************************** 
Etape 41 Cycloperativa - PaPa doualA
Km : 2 / Km Cumulés : 1898
Temps: 13 / Temps Cumulé : 6931
 ************************************************** 
Etape 42 PaPa doualA - Fietskeuken Gent
Km : 60 / Km Cumulés : 1958
Temps: 191 / Temps Cumulé : 7122
 ************************************************** 
Etape 43 Fietskeuken Gent - Atelier MOB
Km : 121 / Km Cumulés : 2079
Temps: 365 / Temps Cumulé : 7487
 ************************************************** 
Etape 44 Atelier MOB - Méli-Vélo
Km : 48 / Km Cumulés : 2127
Temps: 142 / Temps Cumulé : 7629
 ************************************************** 
Etape 45 Méli-Vélo - L'atelier de l'ADAV - Lille
Km : 24 / Km Cumulés : 2151
Temps: 79 / Temps Cumulé : 7708
 ************************************************** 
Etape 46 L'atelier de l'ADAV - Lille - CycloCampus Bethune
Km : 38 / Km Cumulés : 2189
Temps: 133 / Temps Cumulé : 7841
 ************************************************** 
Etape 47 CycloCampus Bethune - Atelier de l'ADAV - Arras
Km : 31 / Km Cumulés : 2220
Temps: 105 / Temps Cumulé : 7946
 ************************************************** 
Etape 48 Atelier de l'ADAV - Arras - Beauvélo
Km : 121 / Km Cumulés : 2341
Temps: 388 / Temps Cumulé : 8334
 ************************************************** 
Etape 49 Beauvélo - Atelier ABC vélo
Km : 38 / Km Cumulés : 2379
Temps: 124 / Temps Cumulé : 8458
 ************************************************** 
Etape 50 Atelier ABC vélo - Les vélos sauvages
Km : 46 / Km Cumulés : 2425
Temps: 162 / Temps Cumulé : 8620
 ************************************************** 
Etape 51 Les vélos sauvages - BicyclAide
Km : 6 / Km Cumulés : 2431
Temps: 23 / Temps Cumulé : 8643
 ************************************************** 
Etape 52 BicyclAide - MDB Asnières
Km : 2 / Km Cumulés : 2433
Temps: 7 / Temps Cumulé : 8650
 ************************************************** 
Etape 53 MDB Asnières - Vélocipaide
Km : 2 / Km Cumulés : 2435
Temps: 9 / Temps Cumulé : 8659
 ************************************************** 
Etape 54 Vélocipaide - Atelier Solidaire de Saint-Ouen
Km : 2 / Km Cumulés : 2437
Temps: 10 / Temps Cumulé : 8669
 ************************************************** 
Etape 55 Atelier Solidaire de Saint-Ouen - Atelier d'autoréparation de vélos nomade de Saint-Denis
Km : 4 / Km Cumulés : 2441
Temps: 14 / Temps Cumulé : 8683
 ************************************************** 
Etape 56 Atelier d'autoréparation de vélos nomade de Saint-Denis - Atelier vélo de l'Attiéké
Km : 0 / Km Cumulés : 2441
Temps: 1 / Temps Cumulé : 8684
 ************************************************** 
Etape 57 Atelier vélo de l'Attiéké - Atelier vélo Dio
Km : 2 / Km Cumulés : 2443
Temps: 8 / Temps Cumulé : 8692
 ************************************************** 
Etape 58 Atelier vélo Dio - La Cyclofficine de Pantin
Km : 6 / Km Cumulés : 2449
Temps: 21 / Temps Cumulé : 8713
 ************************************************** 
Etape 59 La Cyclofficine de Pantin - AICV
Km : 2 / Km Cumulés : 2451
Temps: 7 / Temps Cumulé : 8720
 ************************************************** 
Etape 60 AICV - Atelier vélorutionnaire parisien de la Maison du Vélo
Km : 5 / Km Cumulés : 2456
Temps: 22 / Temps Cumulé : 8742
 ************************************************** 
Etape 61 Atelier vélorutionnaire parisien de la Maison du Vélo - Cyclofficine de Paris-Est
Km : 3 / Km Cumulés : 2459
Temps: 14 / Temps Cumulé : 8756
 ************************************************** 
Etape 62 Cyclofficine de Paris-Est - Atelier vélorutionnaire parisien de Campagne - Squatt 50 r Stendhal
Km : 0 / Km Cumulés : 2459
Temps: 5 / Temps Cumulé : 8761
 ************************************************** 
Etape 63 Atelier vélorutionnaire parisien de Campagne - Squatt 50 r Stendhal - Atelier de Fougères
Km : 2 / Km Cumulés : 2461
Temps: 10 / Temps Cumulé : 8771
 ************************************************** 
Etape 64 Atelier de Fougères - OHCYCLO
Km : 4 / Km Cumulés : 2465
Temps: 20 / Temps Cumulé : 8791
 ************************************************** 
Etape 65 OHCYCLO - Fontenay vélo
Km : 3 / Km Cumulés : 2468
Temps: 12 / Temps Cumulé : 8803
 ************************************************** 
Etape 66 Fontenay vélo - Opti’vélo
Km : 10 / Km Cumulés : 2478
Temps: 34 / Temps Cumulé : 8837
 ************************************************** 
Etape 67 Opti’vélo - Opti’vélo
Km : 11 / Km Cumulés : 2489
Temps: 38 / Temps Cumulé : 8875
 ************************************************** 
Etape 68 Opti’vélo - MDB Sucy
Km : 23 / Km Cumulés : 2512
Temps: 86 / Temps Cumulé : 8961
 ************************************************** 
Etape 69 MDB Sucy - Cyclofficine d'Ivry
Km : 13 / Km Cumulés : 2525
Temps: 42 / Temps Cumulé : 9003
 ************************************************** 
Etape 70 Cyclofficine d'Ivry - Atelier du service jeunesse
Km : 10 / Km Cumulés : 2535
Temps: 44 / Temps Cumulé : 9047
 ************************************************** 
Etape 71 Atelier du service jeunesse - Rayon vert
Km : 3 / Km Cumulés : 2538
Temps: 14 / Temps Cumulé : 9061
 ************************************************** 
Etape 72 Rayon vert - La Boîte à Cycler
Km : 9 / Km Cumulés : 2547
Temps: 39 / Temps Cumulé : 9100
 ************************************************** 
Etape 73 La Boîte à Cycler - (Pas de nom)
Km : 12 / Km Cumulés : 2559
Temps: 46 / Temps Cumulé : 9146
 ************************************************** 
Etape 74 (Pas de nom) - MDB Poissy
Km : 21 / Km Cumulés : 2580
Temps: 72 / Temps Cumulé : 9218
 ************************************************** 
Etape 75 MDB Poissy - Un vélo qui roule
Km : 10 / Km Cumulés : 2590
Temps: 38 / Temps Cumulé : 9256
 ************************************************** 
Etape 76 Un vélo qui roule - Vélo Solidaire
Km : 7 / Km Cumulés : 2597
Temps: 27 / Temps Cumulé : 9283
 ************************************************** 
Etape 77 Vélo Solidaire - Génération Solidaire Vauréal
Km : 6 / Km Cumulés : 2603
Temps: 22 / Temps Cumulé : 9305
 ************************************************** 
Etape 78 Génération Solidaire Vauréal - Guidoline
Km : 90 / Km Cumulés : 2693
Temps: 300 / Temps Cumulé : 9605
 ************************************************** 
Etape 79 Guidoline - La roue libre
Km : 93 / Km Cumulés : 2786
Temps: 298 / Temps Cumulé : 9903
 ************************************************** 
Etape 80 La roue libre - Chez Bidule
Km : 74 / Km Cumulés : 2860
Temps: 252 / Temps Cumulé : 10155
 ************************************************** 
Etape 81 Chez Bidule - Maison du Vélo
Km : 3 / Km Cumulés : 2863
Temps: 9 / Temps Cumulé : 10164
 ************************************************** 
Etape 82 Maison du Vélo - Cyclamaine
Km : 158 / Km Cumulés : 3021
Temps: 527 / Temps Cumulé : 10691
 ************************************************** 
Etape 83 Cyclamaine - Atelier Re'Cycle de l'Argonne
Km : 144 / Km Cumulés : 3165
Temps: 463 / Temps Cumulé : 11154
 ************************************************** 
Etape 84 Atelier Re'Cycle de l'Argonne - Atelier Re'Cycle de la Source
Km : 11 / Km Cumulés : 3176
Temps: 40 / Temps Cumulé : 11194
 ************************************************** 
Etape 85 Atelier Re'Cycle de la Source - La Fabrique
Km : 64 / Km Cumulés : 3240
Temps: 200 / Temps Cumulé : 11394
 ************************************************** 
Etape 86 La Fabrique - Roulement à Bill
Km : 60 / Km Cumulés : 3300
Temps: 189 / Temps Cumulé : 11583
 ************************************************** 
Etape 87 Roulement à Bill - Atelier d'autoréparation La Bricolade
Km : 2 / Km Cumulés : 3302
Temps: 9 / Temps Cumulé : 11592
 ************************************************** 
Etape 88 Atelier d'autoréparation La Bricolade - Atelier du Petit Plateau
Km : 112 / Km Cumulés : 3414
Temps: 367 / Temps Cumulé : 11959
 ************************************************** 
Etape 89 Atelier du Petit Plateau - Atelier Mécanique Vélo
Km : 84 / Km Cumulés : 3498
Temps: 277 / Temps Cumulé : 12236
 ************************************************** 
Etape 90 Atelier Mécanique Vélo - Centre Vélo
Km : 88 / Km Cumulés : 3586
Temps: 285 / Temps Cumulé : 12521
 ************************************************** 
Etape 91 Centre Vélo - Goût d'huile
Km : 59 / Km Cumulés : 3645
Temps: 187 / Temps Cumulé : 12708
 ************************************************** 
Etape 92 Goût d'huile - Vélocampus
Km : 33 / Km Cumulés : 3678
Temps: 110 / Temps Cumulé : 12818
 ************************************************** 
Etape 93 Vélocampus - Atelier Bellevue
Km : 7 / Km Cumulés : 3685
Temps: 28 / Temps Cumulé : 12846
 ************************************************** 
Etape 94 Atelier Bellevue - Atelier Répare Ton Vélo
Km : 23 / Km Cumulés : 3708
Temps: 83 / Temps Cumulé : 12929
 ************************************************** 
Etape 95 Atelier Répare Ton Vélo - La Pompe à Vélo
Km : 92 / Km Cumulés : 3800
Temps: 307 / Temps Cumulé : 13236
 ************************************************** 
Etape 96 La Pompe à Vélo - La petite Rennes
Km : 9 / Km Cumulés : 3809
Temps: 31 / Temps Cumulé : 13267
 ************************************************** 
Etape 97 La petite Rennes - À Vélo Malo
Km : 73 / Km Cumulés : 3882
Temps: 243 / Temps Cumulé : 13510
 ************************************************** 
Etape 98 À Vélo Malo - Vélo utile
Km : 72 / Km Cumulés : 3954
Temps: 252 / Temps Cumulé : 13762
 ************************************************** 
Etape 99 Vélo utile - Répavélo
Km : 114 / Km Cumulés : 4068
Temps: 389 / Temps Cumulé : 14151
 ************************************************** 
Etape 100 Répavélo - Labecane56
Km : 19 / Km Cumulés : 4087
Temps: 73 / Temps Cumulé : 14224
 ************************************************** 
Etape 101 Labecane56 - Vél’Orient - UEAJ - Optim’ism
Km : 38 / Km Cumulés : 4125
Temps: 129 / Temps Cumulé : 14353
 ************************************************** 
Etape 102 Vél’Orient - UEAJ - Optim’ism - CRADE
Km : 51 / Km Cumulés : 4176
Temps: 182 / Temps Cumulé : 14535
 ************************************************** 
Etape 103 CRADE - Atelier Vélautonome de Bigoudenie
Km : 33 / Km Cumulés : 4209
Temps: 119 / Temps Cumulé : 14654
 ************************************************** 
Etape 104 Atelier Vélautonome de Bigoudenie - KILT Atelier Vélo Itinérant
Km : 83 / Km Cumulés : 4292
Temps: 304 / Temps Cumulé : 14958
 ************************************************** 
Etape 105 KILT Atelier Vélo Itinérant - Atelier de la S.A.U.V.A.G.E
Km : 24 / Km Cumulés : 4316
Temps: 98 / Temps Cumulé : 15056
 ************************************************** 
Etape 106 Atelier de la S.A.U.V.A.G.E - Brest à pied et à vélo
Km : 0 / Km Cumulés : 4316
Temps: 2 / Temps Cumulé : 15058
 ************************************************** 
Etape 114 Atelier Vélo Txirrind’ola - RepEyre
Km : 162 / Km Cumulés : 4478
Temps: 524 / Temps Cumulé : 15582
 ************************************************** 
Etape 115 RepEyre - Étu’Récup
Km : 38 / Km Cumulés : 4516
Temps: 115 / Temps Cumulé : 15697
 ************************************************** 
Etape 116 Étu’Récup - Cycles et Manivelles
Km : 6 / Km Cumulés : 4522
Temps: 22 / Temps Cumulé : 15719
 ************************************************** 
Etape 117 Cycles et Manivelles - L’étincelle
Km : 2 / Km Cumulés : 4524
Temps: 7 / Temps Cumulé : 15726
 ************************************************** 
Etape 118 L’étincelle - Récup'R
Km : 1 / Km Cumulés : 4525
Temps: 7 / Temps Cumulé : 15733
 ************************************************** 
Etape 119 Récup'R - Le garage moderne / Les Vélos du Garage
Km : 5 / Km Cumulés : 4530
Temps: 20 / Temps Cumulé : 15753
 ************************************************** 
Etape 120 Le garage moderne / Les Vélos du Garage - Atelier Rustine et Cambouis
Km : 10 / Km Cumulés : 4540
Temps: 35 / Temps Cumulé : 15788
 ************************************************** 
Etape 121 Atelier Rustine et Cambouis - Cyclofficine d’Angoulême
Km : 128 / Km Cumulés : 4668
Temps: 439 / Temps Cumulé : 16227
 ************************************************** 
Etape 122 Cyclofficine d’Angoulême - Vélocyp'Aide
Km : 114 / Km Cumulés : 4782
Temps: 405 / Temps Cumulé : 16632
 ************************************************** 
Etape 123 Vélocyp'Aide - Vélo-Cité 15
Km : 140 / Km Cumulés : 4922
Temps: 535 / Temps Cumulé : 17167
 ************************************************** 
Etape 124 Vélo-Cité 15 - Association EVE
Km : 139 / Km Cumulés : 5061
Temps: 534 / Temps Cumulé : 17701
 ************************************************** 
Etape 125 Association EVE - Atelier vélo d’Aussillon
Km : 149 / Km Cumulés : 5210
Temps: 557 / Temps Cumulé : 18258
 ************************************************** 
Etape 126 Atelier vélo d’Aussillon - La Maison du Vélo Toulouse
Km : 108 / Km Cumulés : 5318
Temps: 335 / Temps Cumulé : 18593
 ************************************************** 
Etape 127 La Maison du Vélo Toulouse - Garage volant de la place Saint-Aubin
Km : 1 / Km Cumulés : 5319
Temps: 5 / Temps Cumulé : 18598
 ************************************************** 
Etape 128 Garage volant de la place Saint-Aubin - Le Labo
Km : 3 / Km Cumulés : 5322
Temps: 11 / Temps Cumulé : 18609
 ************************************************** 
Etape 129 Le Labo - Zone d'INitiative Citoyenne, ZINC
Km : 5 / Km Cumulés : 5327
Temps: 17 / Temps Cumulé : 18626
 ************************************************** 
Etape 130 Zone d'INitiative Citoyenne, ZINC - Vélo’riège
Km : 70 / Km Cumulés : 5397
Temps: 237 / Temps Cumulé : 18863
 ************************************************** 
Etape 131 Vélo’riège - Energie Citoyenne
Km : 147 / Km Cumulés : 5544
Temps: 509 / Temps Cumulé : 19372
 ************************************************** 
Etape 132 Energie Citoyenne - Le Vieux Biclou
Km : 179 / Km Cumulés : 5723
Temps: 571 / Temps Cumulé : 19943
 ************************************************** 
Etape 133 Le Vieux Biclou - Le Vieux Biclou
Km : 3 / Km Cumulés : 5726
Temps: 11 / Temps Cumulé : 19954
 ************************************************** 
Etape 134 Le Vieux Biclou - ConviBicy
Km : 73 / Km Cumulés : 5799
Temps: 231 / Temps Cumulé : 20185
 ************************************************** 
Etape 135 ConviBicy - Roulons à Vélo
Km : 37 / Km Cumulés : 5836
Temps: 118 / Temps Cumulé : 20303
 ************************************************** 
Etape 136 Roulons à Vélo - ADAVA Pays d’Aix
Km : 75 / Km Cumulés : 5911
Temps: 248 / Temps Cumulé : 20551
 ************************************************** 
Etape 137 ADAVA Pays d’Aix - Collectif Vélos en Ville
Km : 32 / Km Cumulés : 5943
Temps: 114 / Temps Cumulé : 20665
 ************************************************** 
Etape 138 Collectif Vélos en Ville - Action Vélo
Km : 17 / Km Cumulés : 5960
Temps: 64 / Temps Cumulé : 20729
 ************************************************** 
Etape 139 Action Vélo - Atelier PharmaCycles 
Km : 48 / Km Cumulés : 6008
Temps: 182 / Temps Cumulé : 20911
 ************************************************** 
Etape 140 Atelier PharmaCycles  - Evaleco
Km : 131 / Km Cumulés : 6139
Temps: 473 / Temps Cumulé : 21384
 ************************************************** 
Etape 141 Evaleco - ViaVélo
Km : 35 / Km Cumulés : 6174
Temps: 119 / Temps Cumulé : 21503
 ************************************************** 
Etape 158 Bikekitchen Augsburg - Vélostation
Km : 301 / Km Cumulés : 6475
Temps: 1011 / Temps Cumulé : 22514
 ************************************************** 
Etape 159 Vélostation - Bretz'Selle
Km : 1 / Km Cumulés : 6476
Temps: 6 / Temps Cumulé : 22520
 ************************************************** 
Etape 160 Bretz'Selle - A’Cro du Vélo
Km : 4 / Km Cumulés : 6480
Temps: 20 / Temps Cumulé : 22540
 ************************************************** 
Etape 161 A’Cro du Vélo - L’Engrenage
Km : 100 / Km Cumulés : 6580
Temps: 347 / Temps Cumulé : 22887
 ************************************************** 
Etape 162 L’Engrenage - Association Colmar à vélo Vélodocteurs
Km : 51 / Km Cumulés : 6631
Temps: 206 / Temps Cumulé : 23093
 ************************************************** 
Etape 163 Association Colmar à vélo Vélodocteurs - Roue pèt'
Km : 31 / Km Cumulés : 6662
Temps: 107 / Temps Cumulé : 23200
 ************************************************** 
Etape 164 Roue pèt' - Maillon solidaire
Km : 59 / Km Cumulés : 6721
Temps: 198 / Temps Cumulé : 23398
 ************************************************** 
Etape 165 Maillon solidaire - Le Black Office
Km : 122 / Km Cumulés : 6843
Temps: 498 / Temps Cumulé : 23896
 ************************************************** 
Etape 166 Le Black Office - Au vieux biclou
Km : 98 / Km Cumulés : 6941
Temps: 410 / Temps Cumulé : 24306
 ************************************************** 
Etape 167 Au vieux biclou - Vélocampus
Km : 7 / Km Cumulés : 6948
Temps: 30 / Temps Cumulé : 24336
 ************************************************** 
Etape 168 Vélocampus - La bécane à Jules
Km : 108 / Km Cumulés : 7056
Temps: 352 / Temps Cumulé : 24688
 ************************************************** 
Etape 169 La bécane à Jules - La rustine
Km : 3 / Km Cumulés : 7059
Temps: 12 / Temps Cumulé : 24700
 ************************************************** 
Etape 170 La rustine - La bécane à Jules
Km : 3 / Km Cumulés : 7062
Temps: 12 / Temps Cumulé : 24712
 ************************************************** 
Etape 171 La bécane à Jules - VELOsurSAONE
Km : 72 / Km Cumulés : 7134
Temps: 227 / Temps Cumulé : 24939
 ************************************************** 
Etape 172 VELOsurSAONE - Véloquirit
Km : 62 / Km Cumulés : 7196
Temps: 212 / Temps Cumulé : 25151
 ************************************************** 
Etape 173 Véloquirit - vélOYO
Km : 59 / Km Cumulés : 7255
Temps: 251 / Temps Cumulé : 25402
 ************************************************** 
Etape 174 vélOYO - Péclôt 13
Km : 74 / Km Cumulés : 7329
Temps: 288 / Temps Cumulé : 25690
 ************************************************** 
Etape 175 Péclôt 13 - Péclôt 13
Km : 0 / Km Cumulés : 7329
Temps: 2 / Temps Cumulé : 25692
 ************************************************** 
Etape 176 Péclôt 13 - La Rustine
Km : 1 / Km Cumulés : 7330
Temps: 5 / Temps Cumulé : 25697
 ************************************************** 
Etape 177 La Rustine - Péclôt 13
Km : 2 / Km Cumulés : 7332
Temps: 9 / Temps Cumulé : 25706
 ************************************************** 
Etape 178 Péclôt 13 - Péclôt 13
Km : 5 / Km Cumulés : 7337
Temps: 27 / Temps Cumulé : 25733
 ************************************************** 
Etape 179 Péclôt 13 - Roule & co
Km : 42 / Km Cumulés : 7379
Temps: 172 / Temps Cumulé : 25905
 ************************************************** 
Etape 180 Roule & co - La vélobricolade de Roue Libre
Km : 52 / Km Cumulés : 7431
Temps: 179 / Temps Cumulé : 26084
 ************************************************** 
Etape 181 La vélobricolade de Roue Libre - uN p'Tit véLo dAnS la Tête
Km : 60 / Km Cumulés : 7491
Temps: 188 / Temps Cumulé : 26272
 ************************************************** 
